import React, { Component } from 'react';
// import logo from './logo.svg';
import Animelist from './anime/anime-list'
import AddAnime from './anime/add-anime'

class App extends Component {
  state  = {
      newAnimes : ['Boruto', 'Shield Hero', 'One Punch Man']
  }
  changeState = (newItems) => {
    if(Array.isArray(newItems))
      this.setState({newAnimes: [...this.state.newAnimes, ...newItems]})
    else
      newItems = newItems.split('-')
    return this.state.newAnimes
  }
  render() {
    return (
      <div className="App">
        <Animelist new_animes={this.state} changeState = {this.changeState}/>
        <AddAnime changeState = {this.changeState}/>
      </div>
    );
  }
}

export default App;
