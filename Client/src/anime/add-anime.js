import React from 'react'
class AddAnime extends React.Component {
    state = {
        newAnime : ''
    }
    AddAnimes= (e) => {
        e.preventDefault()
        return (this.props.changeState([this.state.newAnime]))
    } 
    
    render(){
        return (
            <div>                                
                <form onSubmit={this.AddAnimes}>
                <input type='text' placeholder='Add Anime' name='addNew' onChange={(e) => this.setState({newAnime: e.target.value})}/>
                <input type='submit'/>
                </form>                
                
            </div>            
        )
    }
}
export default AddAnime;