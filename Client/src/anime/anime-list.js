import React from 'react'

class AnimeList extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            animes : ['DBS', 'One Piece', 'My Hero Academia', 'Black Clover', 'Bleach']
        }
    }

    getInitialState() {
        return {
          bgColor: 'red'
        }
    } 

    handleClick = () => {
        this.setState({
          bgColor: '#356382'
        })
      }

    componentWillMount(){
        this.props.changeState(['Mob Psycho', 'World Trigger'])
    }
    getAnimes = () => {
        return (this.state.animes.map((anime, i) => <li id={i} key={i} onClick = {this.handleClick} style={ {backgroundColor: this.state.bgColor}}>{anime}</li>).concat(this.props.new_animes.newAnimes.map((anime, i) => <li id={i + this.state.animes.length} key={i + this.state.animes.length} onClick = {(e) => {console.log(e.target.innerHTML)}}>{anime}</li>)))
    }
    render() {
        return (
            <ul>
                {this.getAnimes()}
            </ul>
        )
    }
}

export default AnimeList;